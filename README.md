# pwgen.rs

Rustacen password generator.

## Installation

```bash
git clone https://framagit.org/UncleSamulus/pwgen.rs.git
cd pwgen.rs
cargo build
```

## Usage

```bash
$ pwgen -n PASSWORD_SIZE
```

