extern crate getopts;
extern crate rand;
use getopts::Options;
use std::env;
use rand::Rng;

fn usage(program: &str, opts: Options) {
    let brief = format!("USAGE: {} [options]", program);
    print!("{}", opts.usage(&brief));
}

/** Generate a password of length n */
fn generate_word(n: i32, contains_symbols: bool) {
    let mut word: String = "".to_owned();
    for i in 0..n {
        let mut rng = rand::thread_rng();
        let c: u8 = rng.gen_range(97..123);
        let c = c as char;
        word.push(c);
    }
    println!("{}", word);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();
    let mut opts = Options::new();
    let mut verbose: bool = false;
    let mut symbols: bool = false;
    opts.optopt("n", "number", "set number of generated password", "NUMBER");
    opts.optflag("y", "symbols", "allow specials symbols in password");
    opts.optflag("v", "verbose", "set verbose mode");
    opts.optflag("h", "help", "show help menu");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!("{}", f.to_string()) }
    };

    if matches.opt_present("v") {
        verbose = true;
    }
    if matches.opt_present("h") {
        usage(&program, opts);
        return;
    }
    if matches.opt_present("y") {
        symbols = true;
    }
    let n = match matches.opt_str("n") {
        Some(n) => { n.parse::<i32>().unwrap() }
        None => { 8 }
    };  
    generate_word(n, symbols);
}
